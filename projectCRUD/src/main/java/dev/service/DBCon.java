package dev.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBCon {

    private static Connection con;

    //tworzenie połączenia
    static Connection getCon() {
        if (con == null) {
            try {
                String DB_URL = "jdbc:hsqldb:hsql://localhost/workdb";
                con = DriverManager.getConnection(DB_URL);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return con;
    }

    public static void closeCon() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else
            System.out.println("Can't close connection (Connection not established)");
    }
}
