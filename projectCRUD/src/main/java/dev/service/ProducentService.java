package dev.service;

import main.java.dev.domain.Producent;

import java.util.ArrayList;

public interface ProducentService {

    long select();

    long insert(Producent producent);

    void insertList(ArrayList<Producent> producentList);

    long update(Producent.Column SET, String columnName, Producent.Column WHERE, String expression);

    long delete(Producent.Column WHERE, String expression);

    long deleteAll();
}
