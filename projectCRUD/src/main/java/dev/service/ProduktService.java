package dev.service;

import main.java.dev.domain.Produkt;

import java.util.ArrayList;

public interface ProduktService {

    long select();

    long insert(Produkt produkt);

    void insertList(ArrayList<Produkt> produktList);

    long update(Produkt.Column SET, String value, Produkt.Column WHERE, String expression);

    long delete(Produkt.Column WHERE, String expression);

    long deleteAll();
}
