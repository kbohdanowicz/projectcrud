package dev.service;

public interface DBActionServiceJDBC {

    void createDB();

    void clearDB();

    int select(String SELECT, String FROM);

    String convertBitToString(String str);
}
