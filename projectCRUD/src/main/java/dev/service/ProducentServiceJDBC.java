package dev.service;

import main.java.dev.domain.Producent;

import java.sql.*;
import java.util.ArrayList;

public class ProducentServiceJDBC implements ProducentService{

    private PreparedStatement selectPst;
    private PreparedStatement insertPst;

    public ProducentServiceJDBC() {
        final String SELECT_STRING = "SELECT * FROM Producent";
        final String INSERT_STRING = "INSERT INTO Producent (nazwa, dataZalozenia, adres) VALUES (?, ?, ?)";
        try {
           selectPst = DBCon.getCon().prepareStatement(SELECT_STRING);
           insertPst = DBCon.getCon().prepareStatement(INSERT_STRING);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public long select() {
        int affectedRowsCount = 0;
        try {
            ResultSet rs = selectPst.executeQuery();
            while (rs.next()) {
                affectedRowsCount++;
                long ID = rs.getLong("ID");
                String nazwa = rs.getString("name");
                Date dataZalozenia = rs.getDate("dataZalozenia");
                String adres = rs.getString("adres");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return affectedRowsCount;
    }

    public void insertList(ArrayList<Producent> produkty) {
        try {
            DBCon.getCon().setAutoCommit(false);
            for(Producent produkt: produkty){
                insertPst.setString(1, produkt.getNazwa());
                insertPst.setDate(2, produkt.getDataZalozenia());
                insertPst.setString(3, produkt.getAdres());

                insertPst.executeUpdate();

                System.out.println("Dodano: " + produkt);
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            DBCon.getCon().commit();
        } catch (SQLException e) {
            try {
                DBCon.getCon().rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    public long insert(Producent producent) {
        try {
            insertPst.setString(1, producent.getNazwa());
            insertPst.setDate(2, producent.getDataZalozenia());
            insertPst.setString(3, producent.getAdres());

            return insertPst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long update(Producent.Column SET, String value, Producent.Column WHERE, String expression) {
        StringBuilder stmt = new StringBuilder();
        stmt.append("UPDATE Producent SET ").append(SET).append(" = ? WHERE ").append(WHERE).append(" = ?");
        try {
            PreparedStatement pstmt = DBCon.getCon().prepareStatement(stmt.toString());
            pstmt.setString(1, value);
            pstmt.setString(2, expression);

            return pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long delete(Producent.Column WHERE, String expression) {
        StringBuilder stmt = new StringBuilder();
        stmt.append("DELETE FROM Producent WHERE ").append(WHERE).append(" = ?");//.name() ???
        try {
            PreparedStatement pstmt = DBCon.getCon().prepareStatement(stmt.toString());
            pstmt.setString(1, expression);

            return pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long deleteAll() {
        String stmt = ("DELETE FROM Producent");
        try {
            PreparedStatement pstmt = DBCon.getCon().prepareStatement(stmt);
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}