package dev.service;

import main.java.dev.domain.Produkt;


import java.sql.*;
import java.util.ArrayList;

public class ProduktServiceJDBC implements dev.service.ProduktService {

    private PreparedStatement selectPst;
    private PreparedStatement insertPst;

    public ProduktServiceJDBC() {
        final String SELECT_STRING = "SELECT * FROM Produkt";
        final String INSERT_STRING = "INSERT INTO Produkt (nazwa, kategoria, waga, cena, czyPromocja, producentId) " +
                                     "VALUES (?, ?, ?, ?, ?, ?)";
        try {
            selectPst = DBCon.getCon().prepareStatement(SELECT_STRING);
            insertPst = DBCon.getCon().prepareStatement(INSERT_STRING);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public long select() {
        int affectedRowsCount = 0;
        try {
            ResultSet rs = selectPst.executeQuery();
            while (rs.next()) {
                affectedRowsCount++;
                long ID = rs.getLong("ID");
                String nazwa = rs.getString("name");
                String kategoria = rs.getString("kategoria");
                int waga = rs.getInt("waga");
                float cena = rs.getInt("cena");
                byte czyPromocja = rs.getByte("czyPromocja");
                long producentID = rs.getInt("producentID");

               //Produkt produkt = new Produkt(ID, nazwa, kategoria, waga, cena, czyPromocja, producentID);
               //result.add(produkt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return affectedRowsCount;
    }

    public void insertList(ArrayList<Produkt> produkty) {
        try {
            DBCon.getCon().setAutoCommit(false);
            for(Produkt produkt: produkty){
                insertPst.setString(1, produkt.getNazwa());
                insertPst.setString(2, produkt.getKategoria());
                insertPst.setInt(3, produkt.getWaga());
                insertPst.setFloat(4, produkt.getCena());
                insertPst.setByte(5, produkt.getCzyPromocja());
                insertPst.setLong(6, produkt.getProducentID());
                insertPst.executeUpdate();

                System.out.println("Dodano: " + produkt);
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            DBCon.getCon().commit();
        } catch (SQLException e) {
            try {
                DBCon.getCon().rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    public long insert(Produkt produkt) {
        try {
            insertPst.setString(1, produkt.getNazwa());
            insertPst.setString(2, produkt.getKategoria());
            insertPst.setInt(3, produkt.getWaga());
            insertPst.setFloat(4, produkt.getCena());
            insertPst.setByte(5, produkt.getCzyPromocja());
            insertPst.setLong(6, produkt.getProducentID());

            return insertPst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long update(Produkt.Column SET, String value, Produkt.Column WHERE, String expression) {
        StringBuilder stmt = new StringBuilder();
        stmt.append("UPDATE Produkt SET ").append(SET).append(" = ? WHERE ").append(WHERE).append(" = ?");
        //if (SET.name().equals("czyPromocja") && !(value.equals("0") || value.equals("1")))
        //    throw new IllegalArgumentException("Niepoprawna wartość atrybutu \"czyPromocja\"");
        //else {
            try {
                System.out.println(stmt.toString());
                PreparedStatement pstmt = DBCon.getCon().prepareStatement(stmt.toString());
                pstmt.setString(1, value);
                pstmt.setString(2, expression);

                return pstmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        //}
        return 0;
    }

    public long delete(Produkt.Column WHERE, String expression) {
        StringBuilder stmt = new StringBuilder();
        stmt.append("DELETE FROM Produkt WHERE ").append(WHERE).append(" = ?");
        try {
            PreparedStatement pstmt = DBCon.getCon().prepareStatement(stmt.toString());
            pstmt.setString(1, expression);
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long deleteAll() {
        String stmt = ("DELETE FROM Produkt");
        try {
            PreparedStatement pstmt = DBCon.getCon().prepareStatement(stmt);
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    //count
    //order by
}
