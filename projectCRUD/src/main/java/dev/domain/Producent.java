package main.java.dev.domain;

import java.sql.Date;

public class Producent {

    private long ID;
    private String nazwa;
    private Date dataZalozenia;
    private String adres;

    public enum Column{ID, NAZWA, DATAZALOZENIA, ADRES}

    public Producent(long ID, String nazwa, Date dataZalozenia, String adres) {
        this.ID = ID;
        this.nazwa = nazwa;
        this.dataZalozenia = dataZalozenia;
        this.adres = adres;
    }

    public Producent(String nazwa, Date dataZalozenia, String adres) {
        this.nazwa = nazwa;
        this.dataZalozenia = dataZalozenia;
        this.adres = adres;
    }

    public String getNazwa() {
        return nazwa;
    }

    public Date getDataZalozenia() {
        return dataZalozenia;
    }

    public String getAdres() {
        return adres;
    }
}
