package main.java.dev.domain;

public class Produkt {

    private long ID;
    private String nazwa;
    private String kategoria;
    private int waga;
    private float cena;
    private byte czyPromocja;
    private long producentID;

    public enum Column{ID, NAZWA, KATEGORIA, WAGA, CENA, CZYPROMOCJA, PRODUCENTID}

    public Produkt(long ID, String nazwa, String kategoria, int waga, float cena, byte czyPromocja, long producentID) {
        this.ID = ID;
        this.nazwa = nazwa;
        this.kategoria = kategoria;
        this.waga = waga;
        this.cena = cena;
        this.czyPromocja = czyPromocja;
        this.producentID = producentID;
    }

    public Produkt(String nazwa, String kategoria, int waga, float cena, byte czyPromocja, long producentID) {
        /*
        if (waga <= 0)
            throw new IllegalArgumentException("Niepoprawna wartość atrybutu \"waga\"");
        if (cena < 0)
            throw new IllegalArgumentException("Niepoprawna wartość atrybutu \"cena\"");
        if (!(czyPromocja == (byte) 0 || czyPromocja == (byte) 1))
            throw new IllegalArgumentException("Niepoprawna wartość atrybutu \"czyPromocja\"");
        */
        this.nazwa = nazwa;
        this.kategoria = kategoria;
        this.waga = waga;
        this.cena = cena;
        this.czyPromocja = czyPromocja;
        this.producentID = producentID;
    }

    public String getNazwa() {
        return nazwa;
    }

    public String getKategoria() {
        return kategoria;
    }

    public int getWaga() {
        return waga;
    }

    public float getCena() {
        return cena;
    }

    public byte getCzyPromocja() {
        return czyPromocja;
    }

    public long getProducentID() {
        return producentID;
    }
}
