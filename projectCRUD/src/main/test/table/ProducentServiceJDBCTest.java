package table;

import main.java.dev.domain.Producent;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import dev.service.DBActionService;
import dev.service.DBCon;
import dev.service.ProducentServiceJDBC;

import java.sql.Date;

import static org.junit.Assert.assertEquals;

public class ProducentServiceJDBCTest {

    private DBActionService dbAction = new DBActionService();
    private ProducentServiceJDBC producentServiceJDBC = new ProducentServiceJDBC();

    @Before
    public void clearDB() {
        dbAction.clearDB();
        dbAction.createDB();
        producentServiceJDBC.insert(
                new Producent("Herbapol", Date.valueOf("1950-07-12"),"Szkolna 17 45-535 Białystok"));
    }

    @AfterClass
    public static void closeCon() {
        DBCon.closeCon();
    }

    @Test
    public void insertTest() {
        assertEquals(1,
                producentServiceJDBC.insert(
                        new Producent("Herbapol", Date.valueOf("1950-07-12"),"Szkolna 17 45-535 Białystok")));
    }

    @Test
    public void updateTest() {
        assertEquals(1, producentServiceJDBC.update(
                Producent.Column.ID,"Szkolna 10 45-527 Białystok", Producent.Column.NAZWA,"Herbapol"));
    }

    @Test
    public void deleteTest() {
        assertEquals(1, producentServiceJDBC.delete(Producent.Column.ID,"Herbapol"));
    }

    @Test
    public void selectTest(){
        assertEquals(1, producentServiceJDBC.select());
    }
}

