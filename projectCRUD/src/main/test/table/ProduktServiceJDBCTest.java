package table;

import main.java.dev.domain.Producent;
import main.java.dev.domain.Produkt;
import dev.service.DBActionService;
import dev.service.DBCon;
import dev.service.ProducentServiceJDBC;
import dev.service.ProduktServiceJDBC;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.assertEquals;

public class ProduktServiceJDBCTest {

    private DBActionService dbAction = new DBActionService();
    private ProducentServiceJDBC producentServiceJDBC = new ProducentServiceJDBC();
    private ProduktServiceJDBC produktServiceJDBC = new ProduktServiceJDBC();

    @Before
    public void before() {
        dbAction.clearDB();
        dbAction.createDB();

        producentServiceJDBC.insert(
                new Producent("Herbapol", Date.valueOf("1950-07-12"), "Szkolna 17 Białystok 45-535"));
        producentServiceJDBC.insert(
                new Producent("Tetley", Date.valueOf("1920-05-18"),"Kościelna 7 34-100 Wadowice"));

        produktServiceJDBC.insert(
                new Produkt("Czarna Herbata", "Herbata", 50, 9.99f, (byte) 0, 1));
        produktServiceJDBC.insert(
                new Produkt("Zielona Herbata","Herbata",50,12.99f, (byte) 0,1));

        produktServiceJDBC.insert(
                new Produkt("Zielona Herbata","Herbata",50,9.99f, (byte) 0,2));
        produktServiceJDBC.insert(
                new Produkt("Biała Herbata","Herbata",50,12.99f, (byte) 0,2));
        produktServiceJDBC.insert(
                new Produkt("Czerwona Herbata","Herbata",50,15.99f, (byte) 0,2));
    }

    @AfterClass
    public static void afterClass() {
        DBCon.closeCon();
    }

    @Test
    public void insertTest() {
        assertEquals(1, produktServiceJDBC.insert(
                new Produkt("Zielona Herbata","Herbata",100,9.99f, (byte) 0,1)));
    }

    @Test
    public void updateTest() {
        assertEquals(1,
                produktServiceJDBC.update(
                        Produkt.Column.CENA, "9.99",Produkt.Column.NAZWA,"Czarna Herbata"));
    }

    @Test
    public void deleteTest() {
       assertEquals(1,
               produktServiceJDBC.delete(Produkt.Column.NAZWA,"Czarna Herbata"));
    }

    @Test
    public void selectTest(){
        assertEquals(5, produktServiceJDBC.select());
    }

    @Test
    public void updateWhereProducentTest(){
        assertEquals(3, produktServiceJDBC.update(
                Produkt.Column.CZYPROMOCJA, "1", Produkt.Column.PRODUCENTID,"2"));
    }

    @Test
    public void selectWhereCenaTest(){
        assertEquals(3, produktServiceJDBC.select());
    }

    @Test
    public void selectWhereProducentTest(){
        assertEquals(3, produktServiceJDBC.select());
    }

    /*
           (SELECT cena FROM Produkt)
    */

    @Test // TODO: 2019-11-15
    public void updatePriceTest(){
        //assertEquals(5, Produkt.update("cena", expression,"producentId","2"));
        assertEquals(5, produktServiceJDBC.select());
    }

    @Test
    public void selectJoinTest(){
        assertEquals(5, produktServiceJDBC.select());
    }

}

//trigger on delete cascade
