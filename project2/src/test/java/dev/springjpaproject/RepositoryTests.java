package dev.springjpaproject;

import static org.hamcrest.CoreMatchers.*;

import dev.springjpaproject.domain.Adres;
import dev.springjpaproject.domain.Produkt;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import dev.springjpaproject.domain.Producent;
import dev.springjpaproject.service.AdresRepository;
import dev.springjpaproject.service.ProducentRepository;
import dev.springjpaproject.service.ProduktRepository;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
//@Sql("classpath:./insert.sql")
class RepositoryTests {

	@Autowired
	AdresRepository adresRepo;

	@Autowired
	ProducentRepository producentRepo;

	@Autowired
	ProduktRepository produktRepo;

	@BeforeEach
	void setUp() {
		if (!adresRepo.findAll().isEmpty())
			adresRepo.deleteAll();
		if (!producentRepo.findAll().isEmpty())
			producentRepo.deleteAll();
		if (!produktRepo.findAll().isEmpty())
			produktRepo.deleteAll();
	}

	@Test
	void repositoryTest() {
		assertNotNull(produktRepo);
		assertNotNull(adresRepo);
		assertNotNull(producentRepo);
	}

	@Test
	void insertTest() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt testProdukt = new Produkt("Zielona Herbata", "Herbata", 50,
				new BigDecimal(7.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(testProdukt);

		assertTrue(adresRepo.findById(testAdres.getId()).isPresent());
		assertEquals(adresRepo.findById(testAdres.getId()).get(), testAdres);

		assertTrue(producentRepo.findById(testProducent.getId()).isPresent());
		assertEquals(producentRepo.findById(testProducent.getId()).get(), testProducent);

		assertTrue(produktRepo.findById(testProdukt.getId()).isPresent());
		assertEquals(produktRepo.findById(testProdukt.getId()).get(), testProdukt);
	}

	@Test
	void selectTest() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt testProdukt1 = new Produkt("Zielona Herbata", "Herbata", 50,
				new BigDecimal(7.99), true, testProducent);
		Produkt testProdukt2 = new Produkt("Czarna Herbata", "Herbata", 50,
				new BigDecimal(7.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(testProdukt1);
		produktRepo.save(testProdukt2);

		assertTrue(produktRepo.findById(testProdukt1.getId()).isPresent());
		assertEquals(produktRepo.findById(testProdukt1.getId()).get(), testProdukt1);

		assertTrue(produktRepo.findById(testProdukt2.getId()).isPresent());
		assertEquals(produktRepo.findById(testProdukt2.getId()).get(), testProdukt2);
	}

	@Test
	void updateTest() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt testProdukt1 = new Produkt("Zielona Herbata", "Herbata", 50,
				new BigDecimal(7.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(testProdukt1);

		assertTrue(produktRepo.findById(testProdukt1.getId()).isPresent());
		Produkt testProdukt2 = produktRepo.getOne(testProdukt1.getId());
		testProdukt2.setNazwa("Niebieska Herbata");
		produktRepo.save(testProdukt2);
		assertEquals(produktRepo.findAll().size(), 1);

		assertTrue(produktRepo.findById(testProdukt2.getId()).isPresent());
		assertEquals(produktRepo.findById(testProdukt2.getId()).get().getNazwa(), testProdukt2.getNazwa());
	}

	@Test
	void deleteTest() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt testProdukt = new Produkt("Zielona Herbata", "Herbata", 50,
				new BigDecimal(7.99), true, testProducent
		);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(testProdukt);


		produktRepo.deleteAll();
		assertTrue(produktRepo.findAll().isEmpty());
		assertFalse(producentRepo.findAll().isEmpty());
		assertFalse(adresRepo.findAll().isEmpty());


		produktRepo.save(testProdukt);
		producentRepo.deleteAll();
		assertTrue(produktRepo.findAll().isEmpty());
		assertTrue(producentRepo.findAll().isEmpty());
		assertTrue(adresRepo.findAll().isEmpty());


		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		adresRepo.deleteAll();
		assertTrue(producentRepo.findAll().isEmpty());
		assertTrue(adresRepo.findAll().isEmpty());
	}

	// Query methods with 3+ keywords
	@Test
	void customQueryOne() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt herbataLekkaTaniaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 50,
				new BigDecimal(6.99), true, testProducent);
		Produkt yerbaCiezkaDroga = new Produkt("Taragui", "Yerba Mate", 100,
				new BigDecimal(8.99), false, testProducent);
		Produkt herbataCiezkaDrogaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 100,
				new BigDecimal(12.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(herbataLekkaTaniaNaPromocji);
		produktRepo.save(yerbaCiezkaDroga);
		produktRepo.save(herbataCiezkaDrogaNaPromocji);

		// 1)
		List<Produkt> actual = produktRepo.findByNazwaLikeAndCenaLessThanOrderByWagaAsc(
				"Czarna Herbata", new BigDecimal(11.99));
		List<Produkt> expected = Collections.singletonList(herbataLekkaTaniaNaPromocji);
		assertThat(actual, is(expected));
	}

	@Test
	void customQueryTwo() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt herbataLekkaTaniaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 50,
				new BigDecimal(6.99), true, testProducent);
		Produkt yerbaCiezkaDroga = new Produkt("Taragui", "Yerba Mate", 100,
				new BigDecimal(8.99), false, testProducent);
		Produkt herbataCiezkaDrogaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 100,
				new BigDecimal(12.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(herbataLekkaTaniaNaPromocji);
		produktRepo.save(yerbaCiezkaDroga);
		produktRepo.save(herbataCiezkaDrogaNaPromocji);

		// 2)
		List<Produkt> actual = produktRepo.findByCenaLessThanEqualAndCzyPromocjaTrueOrderByCenaAsc(
				new BigDecimal(11.99));
		List<Produkt> expected = Collections.singletonList(herbataLekkaTaniaNaPromocji);
		assertThat(actual, is(expected));
	}

	@Test
	void customQueryThree() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt herbataLekkaTaniaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 50,
				new BigDecimal(6.99), true, testProducent);
		Produkt yerbaCiezkaDroga = new Produkt("Taragui", "Yerba Mate", 100,
				new BigDecimal(8.99), false, testProducent);
		Produkt herbataCiezkaDrogaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 100,
				new BigDecimal(12.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(herbataLekkaTaniaNaPromocji);
		produktRepo.save(yerbaCiezkaDroga);
		produktRepo.save(herbataCiezkaDrogaNaPromocji);

		// 3)
		List<Produkt> actual = produktRepo.findByKategoriaEqualsAndWagaGreaterThanEqualOrderByCenaDesc(
				"Herbata", 100);
		List<Produkt> expected = Collections.singletonList(herbataCiezkaDrogaNaPromocji);
		assertThat(actual, is(expected));
	}

	@Test
	void customQueryFour() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt herbataLekkaTaniaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 50,
				new BigDecimal(6.99), true, testProducent);
		Produkt yerbaCiezkaDroga = new Produkt("Taragui", "Yerba Mate", 100,
				new BigDecimal(8.99), false, testProducent);
		Produkt herbataCiezkaDrogaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 100,
				new BigDecimal(12.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(herbataLekkaTaniaNaPromocji);
		produktRepo.save(yerbaCiezkaDroga);
		produktRepo.save(herbataCiezkaDrogaNaPromocji);

		// 4)
		List<Produkt> actual = produktRepo.findByCenaBetweenAndKategoriaContainingAndCzyPromocjaOrderByCenaAsc(
				new BigDecimal(7.99), new BigDecimal(11.99),
				"Yerba Mate", false);
		List<Produkt> expected = Collections.singletonList(yerbaCiezkaDroga);
		assertThat(actual, is(expected));
	}
	//

	@Test
	void namedQueryOne() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt herbataLekkaTaniaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 50,
				new BigDecimal(6.99), true, testProducent);
		Produkt yerbaCiezkaDroga = new Produkt("Taragui", "Yerba Mate", 100,
				new BigDecimal(8.99), false, testProducent);
		Produkt herbataCiezkaDrogaNaPromocji = new Produkt("Niebieska Herbata", "Herbata", 100,
				new BigDecimal(12.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(herbataLekkaTaniaNaPromocji);
		produktRepo.save(yerbaCiezkaDroga);
		produktRepo.save(herbataCiezkaDrogaNaPromocji);

		// 1) Named query with parameter
		List<Produkt> actual = produktRepo.findAllAboveWaga(50);
		List<Produkt> expected = Arrays.asList(yerbaCiezkaDroga, herbataCiezkaDrogaNaPromocji);
		assertThat(actual, is(expected));
	}

	@Test
	void namedQueryTwo() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt herbataLekkaTaniaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 50,
				new BigDecimal(6.99), true, testProducent);
		Produkt yerbaCiezkaDroga = new Produkt("Taragui", "Yerba Mate", 100,
				new BigDecimal(8.99), false, testProducent);
		Produkt herbataCiezkaDrogaNaPromocji = new Produkt("Niebieska Herbata", "Herbata", 100,
				new BigDecimal(12.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(herbataLekkaTaniaNaPromocji);
		produktRepo.save(yerbaCiezkaDroga);
		produktRepo.save(herbataCiezkaDrogaNaPromocji);

		// 2) Named query without parameter
		List<Produkt> actual = produktRepo.findAllCzyPromocja();
		List<Produkt> expected = Arrays.asList(herbataLekkaTaniaNaPromocji, herbataCiezkaDrogaNaPromocji);
		assertThat(actual, is(expected));
	}

	@Test
	void queryOne() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt herbataLekkaTaniaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 50,
				new BigDecimal(6.99), true, testProducent);
		Produkt yerbaCiezkaDroga = new Produkt("Taragui", "Yerba Mate", 100,
				new BigDecimal(8.99), false, testProducent);
		Produkt herbataCiezkaDrogaNaPromocji = new Produkt("Niebieska Herbata", "Herbata", 100,
				new BigDecimal(12.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(herbataLekkaTaniaNaPromocji);
		produktRepo.save(yerbaCiezkaDroga);
		produktRepo.save(herbataCiezkaDrogaNaPromocji);

		// 3) Query with parameter
		List<Produkt> actual = produktRepo.findAllBelowCena(new BigDecimal(11.99));
		List<Produkt> expected = Arrays.asList(herbataLekkaTaniaNaPromocji, yerbaCiezkaDroga);
		assertThat(actual, is(expected));
	}

	@Test
	void queryTwo() {
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt herbataLekkaTaniaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 50,
				new BigDecimal(6.99), true, testProducent);
		Produkt yerbaCiezkaDroga = new Produkt("Taragui", "Yerba Mate", 100,
				new BigDecimal(8.99), false, testProducent);
		Produkt herbataCiezkaDrogaNaPromocji = new Produkt("Niebieska Herbata", "Herbata", 100,
				new BigDecimal(12.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(herbataLekkaTaniaNaPromocji);
		produktRepo.save(yerbaCiezkaDroga);
		produktRepo.save(herbataCiezkaDrogaNaPromocji);

		// 4) Query without parameter
		List<Produkt> actual = produktRepo.findAllTeas();
		List<Produkt> expected = Arrays.asList(herbataLekkaTaniaNaPromocji, herbataCiezkaDrogaNaPromocji);
		assertThat(actual, is(expected));
	}

	@Test
	void transactionTest(){
		Adres testAdres = new Adres("Polska", "Gdynia", "Polna", "4", "23-232");
		Producent testProducent = new Producent("Herbapol", Date.valueOf("1950-01-01"), testAdres);
		Produkt herbataLekkaTaniaNaPromocji = new Produkt("Czarna Herbata", "Herbata", 50,
				new BigDecimal(6.99), true, testProducent);
		Produkt yerbaCiezkaDroga = new Produkt("Taragui", "Yerba Mate", 100,
				new BigDecimal(8.99), false, testProducent);
		Produkt herbataCiezkaDrogaNaPromocji = new Produkt("Niebieska Herbata", "Herbata", 100,
				new BigDecimal(12.99), true, testProducent);
		adresRepo.save(testAdres);
		producentRepo.save(testProducent);
		produktRepo.save(herbataLekkaTaniaNaPromocji);
		produktRepo.save(yerbaCiezkaDroga);
		produktRepo.save(herbataCiezkaDrogaNaPromocji);

		produktRepo.deleteProduktsBelowCena(new BigDecimal(9.99));

		List<Produkt> actual = produktRepo.findAll();
		List<Produkt> expected = Collections.singletonList(herbataCiezkaDrogaNaPromocji);
		assertThat(actual, is(expected));
	}
}