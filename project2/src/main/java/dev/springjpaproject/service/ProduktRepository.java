package dev.springjpaproject.service;

import java.math.BigDecimal;
import java.util.List;

import dev.springjpaproject.domain.Produkt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface ProduktRepository extends JpaRepository<Produkt, Long>{

    <P extends Produkt> P save(P player);

    void delete(Produkt produkt);

    // 1) Named query with parameter
    List<Produkt> findAllAboveWaga(int waga);

    // 2) Named query without parameter
    List<Produkt> findAllCzyPromocja();

    // 3) Query with parameter
    @Query(value = "SELECT p FROM Produkt p WHERE cena < ?1 ORDER BY cena")
    List<Produkt> findAllBelowCena(BigDecimal cena);

    // 4) Query without parameter
    @Query(value = "SELECT p FROM Produkt p WHERE kategoria LIKE 'Herbata' ORDER BY cena")
    List<Produkt> findAllTeas();

    // 1) 2) 3) 4) Query methods with 3+ keywords
    List<Produkt> findByNazwaLikeAndCenaLessThanOrderByWagaAsc(
            String nazwa, BigDecimal cena);

    List<Produkt> findByCenaLessThanEqualAndCzyPromocjaTrueOrderByCenaAsc(BigDecimal cena);

    List<Produkt> findByKategoriaEqualsAndWagaGreaterThanEqualOrderByCenaDesc(
            String kategoria, int waga);

    List<Produkt> findByCenaBetweenAndKategoriaContainingAndCzyPromocjaOrderByCenaAsc(
            BigDecimal cena, BigDecimal cena2, String kategoria, Boolean czyPromocja);

    // transaction
    @Modifying
    @Transactional
    @Query("delete from Produkt p where p.cena < ?1")
    void deleteProduktsBelowCena(BigDecimal cena);
}
