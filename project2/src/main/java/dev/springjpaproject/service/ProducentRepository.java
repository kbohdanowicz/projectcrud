package dev.springjpaproject.service;

import org.springframework.data.jpa.repository.JpaRepository;
import dev.springjpaproject.domain.Producent;

public interface ProducentRepository extends JpaRepository<Producent, Long> {

    <P extends Producent> P save(P producent);

    void delete(Producent producent);
}
