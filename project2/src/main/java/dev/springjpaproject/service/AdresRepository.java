package dev.springjpaproject.service;

import dev.springjpaproject.domain.Adres;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdresRepository extends JpaRepository<Adres, Long>{

    <A extends Adres> A save(A adres);

    void delete(Adres adres);
}
