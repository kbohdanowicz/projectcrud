package dev.springjpaproject.domain;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "Producent")
@NamedQueries({
        // 1) named query with parameter
        @NamedQuery(name = "Producent.findAllOlderThan",
                query = "SELECT p FROM Producent p WHERE p.dataZalozenia > '1930-01-01' ORDER BY p.nazwa DESC")
})
public class Producent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "nazwa")
    private String nazwa;

    @Column(name = "dataZalozenia")
    private Date dataZalozenia;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Adres adres;

    @OneToMany(mappedBy = "producent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Produkt> produkts;

    public Producent() {}

    public Producent(String nazwa, Date dataZalozenia, Adres adres) {
        this.nazwa = nazwa;
        this.dataZalozenia = dataZalozenia;

        adres.setProducent(this);
        setAdres(adres);

        produkts = new ArrayList<>();
    }


    public void addProdukt(Produkt produkt) {
        produkt.setProducent(this);
        produkts.add(produkt);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Date getDataZalozenia() {
        return dataZalozenia;
    }

    public void setDataZalozenia(Date dataZalozenia) {
        this.dataZalozenia = dataZalozenia;
    }

    public Adres getAdres() {
        return adres;
    }

    public void setAdres(Adres adres) {
        this.adres = adres;
    }

    public List<Produkt> getProdukts() {
        return produkts;
    }

    public void setProdukts(List<Produkt> produkts) {
        this.produkts = produkts;
    }

    @Override
    public String toString() {
        return "Producent{" +
                "id=" + id +
                ", nazwa='" + nazwa + '\'' +
                ", dataZalozenia=" + dataZalozenia +
                ", adres=" + adres +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        Producent producent = (Producent) o;
        return Objects.equals(id, producent.id) &&
                Objects.equals(nazwa, producent.nazwa) &&
                Objects.equals(dataZalozenia, producent.dataZalozenia) &&
                Objects.equals(adres.getId(), producent.adres.getId());
    }
}
