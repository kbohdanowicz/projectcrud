package dev.springjpaproject.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "Produkt")
@NamedQueries({
        // 1) Named query with parameter
        @NamedQuery(name = "Produkt.findAllAboveWaga",
                query = "SELECT p FROM Produkt p WHERE waga > ?1 ORDER BY waga"),
        // 2) Named query without parameter
        @NamedQuery(name = "Produkt.findAllCzyPromocja",
                query = "SELECT p FROM Produkt p WHERE czyPromocja = true ORDER BY nazwa"),
})
public class Produkt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "nazwa")
    private String nazwa;

    @Column(name = "kategoria")
    private String kategoria;

    @Column(name = "waga")
    private int waga;

    @Column(name = "cena", precision = 10, scale = 2)// 10 znaków, 2 cyfru po przecinku
    private BigDecimal cena;

    @Column(name = "czyPromocja")
    private Boolean czyPromocja;

    @ManyToOne
    private Producent producent;

    public Produkt() {}

    public Produkt(String nazwa, String kategoria, int waga, BigDecimal cena, Boolean czyPromocja, Producent producent) {
        this.nazwa = nazwa;
        this.kategoria = kategoria;
        this.waga = waga;
        this.cena = cena.setScale(2, RoundingMode.HALF_UP);
        this.czyPromocja = czyPromocja;

        setProducent(producent);
        List<Produkt> updatedProdukts = producent.getProdukts();
        updatedProdukts.add(this);
        producent.setProdukts(updatedProdukts);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getKategoria() {
        return kategoria;
    }

    public void setKategoria(String kategoria) {
        this.kategoria = kategoria;
    }

    public int getWaga() {
        return waga;
    }

    public void setWaga(int waga) {
        this.waga = waga;
    }

    public BigDecimal getCena() {
        return cena;
    }

    public void setCena(BigDecimal cena) {
        this.cena = cena;
    }

    public Boolean getCzyPromocja() {
        return czyPromocja;
    }

    public void setCzyPromocja(Boolean czyPromocja) {
        this.czyPromocja = czyPromocja;
    }

    public Producent getProducent() {
        return producent;
    }

    public void setProducent(Producent producent) {
        this.producent = producent;
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "id=" + id +
                ", nazwa='" + nazwa + '\'' +
                ", kategoria='" + kategoria + '\'' +
                ", waga=" + waga +
                ", cena=" + cena +
                ", czyPromocja=" + czyPromocja +
                ", producent=" + producent +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        DecimalFormat df = new DecimalFormat("0.00");
        Produkt produkt = (Produkt) o;
        return  Objects.equals(id, produkt.id) &&
                Objects.equals(nazwa, produkt.nazwa) &&
                Objects.equals(kategoria, produkt.kategoria) &&
                waga == produkt.waga &&
                Objects.equals(df.format(cena), df.format(produkt.cena)) &&
                Objects.equals(czyPromocja, produkt.czyPromocja) &&
                Objects.equals(producent, produkt.producent);
    }
}
