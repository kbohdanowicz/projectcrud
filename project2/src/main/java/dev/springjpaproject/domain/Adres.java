package dev.springjpaproject.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Adres")
public class Adres {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "kraj")
    private String kraj;

    @Column(name = "miasto")
    private String miasto;

    @Column(name = "ulica")
    private String ulica;

    @Column(name = "numer")
    private String numer;

    @Column(name = "kod")
    private String kod;

    @OneToOne(mappedBy = "adres", cascade = CascadeType.ALL, orphanRemoval = true)
    private Producent producent;

    public Adres() {}

    public Adres(String kraj, String miasto, String ulica, String numer, String kod) {
        this.kraj = kraj;
        this.miasto = miasto;
        this.ulica = ulica;
        this.numer = numer;
        this.kod = kod;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKraj() {
        return kraj;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getNumer() {
        return numer;
    }

    public void setNumer(String numer) {
        this.numer = numer;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public Producent getProducent() {
        return producent;
    }

    public void setProducent(Producent producent) {
        this.producent = producent;
    }

    @Override
    public String toString() {
        return "Adres{" +
                "id=" + id +
                ", kraj='" + kraj + '\'' +
                ", miasto='" + miasto + '\'' +
                ", ulica='" + ulica + '\'' +
                ", numer='" + numer + '\'' +
                ", kod='" + kod + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        Adres adres = (Adres) o;
        return Objects.equals(id, adres.id) &&
                Objects.equals(kraj, adres.kraj) &&
                Objects.equals(miasto, adres.miasto) &&
                Objects.equals(ulica, adres.ulica) &&
                Objects.equals(numer, adres.numer) &&
                Objects.equals(kod, adres.kod) &&
                Objects.equals(producent, adres.producent);
    }
}
